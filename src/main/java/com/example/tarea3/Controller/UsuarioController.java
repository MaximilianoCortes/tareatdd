package com.example.tarea3.Controller;

import com.example.tarea3.DTO.UsuarioDTO;
import com.example.tarea3.utils.ValidarUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class UsuarioController {

    private final ValidarUsuario validarUsuario;

    @Autowired
    public UsuarioController(ValidarUsuario validarUsuario) {
        this.validarUsuario = validarUsuario;
    }

    @PostMapping("/crear-usuario")
    public String crearUsuario(@RequestBody UsuarioDTO usuarioDTO) {
        StringBuilder mensaje = new StringBuilder();

        Map<String, String> validacionNombre = validarUsuario.validarNombre(usuarioDTO.getNombre());
        mensaje.append("nombre: ").append(validacionNombre.get("estado")).append("\n");

        Map<String, String> validacionApellidoPaterno = validarUsuario.validarNombre(usuarioDTO.getApellidoPaterno());
        mensaje.append("Apellido Paterno: ").append(validacionApellidoPaterno.get("estado")).append("\n");

        Map<String, String> validacionApellidoMaterno = validarUsuario.validarNombre(usuarioDTO.getApellidoMaterno());
        mensaje.append("Apellido Materno: ").append(validacionApellidoMaterno.get("estado")).append("\n");

        Map<String, String> validacionRut = validarUsuario.validarRut(usuarioDTO.getRut());
        mensaje.append("rut: ").append(validacionRut.get("estado")).append("\n");

        Map<String, String> validacionNumero = validarUsuario.validarNumero(usuarioDTO.getNumero());
        mensaje.append("número de teléfono: ").append(validacionNumero.get("estado")).append("\n");

        Map<String, String> validacionEdad = validarUsuario.validarEdad(usuarioDTO.getEdad());
        mensaje.append("edad: ").append(validacionEdad.get("estado")).append("\n");

        return mensaje.toString();
    }




}
