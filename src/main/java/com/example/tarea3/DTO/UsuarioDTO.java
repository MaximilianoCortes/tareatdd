package com.example.tarea3.DTO;

import lombok.Data;

@Data
public class UsuarioDTO {
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String rut;
    private String numero;
    private int edad;
}

