package com.example.tarea3.utils;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ValidarUsuario {
    public Map<String, String> validarNombre(String nombre) {

        Map<String, String> resultado = new HashMap<>();

        if (nombre.matches(".*\\d.*")) {
            resultado.put("estado", "El nombre/apellido no debe contener números");
        } else {
            resultado.put("estado", "valido");
        }

        return resultado;
    }

    public Map<String, String> validarNumero(String numero) {
        Map<String, String> resultado = new HashMap<>();

        // Eliminar espacios en blanco
        String numeroFormateado = numero.replaceAll("\\s+", "");

        // Verificar el formato del número de teléfono
        String regex = "\\+56(9|2)\\d{8}";

        if (!numeroFormateado.matches(regex)) {
            resultado.put("estado", "El número de teléfono no cumple con el formato válido (+56 9XXXXXXXX)");
        } else {
            resultado.put("estado", "valido");
        }

        return resultado;
    }



    public Map<String, String> validarRut(String rut) {
        Map<String, String> resultado = new HashMap<>();

        String rutRegex = "^\\d{1,2}(\\.\\d{3}){2}-[\\dkK]$";

        if (!rut.matches(rutRegex)) {
            resultado.put("estado", "El RUT no cumple con el formato válido (ejemplo: 12.345.678-9)");
        } else {
            // Eliminar puntos y guión del RUT para calcular el dígito verificador
            String rutSinGuion = rut.replaceAll("[.\\-]", "");

            // Separar el cuerpo del RUT y el dígito verificador
            String cuerpo = rutSinGuion.substring(0, rutSinGuion.length() - 1);
            char digitoVerificador = rutSinGuion.charAt(rutSinGuion.length() - 1);

            // Calcular el dígito verificador esperado
            char digitoVerificadorEsperado = calcularDigitoVerificador(cuerpo);

            if (digitoVerificador != digitoVerificadorEsperado) {
                resultado.put("estado", "El dígito verificador no es válido");
            } else {
                resultado.put("estado", "valido");
            }
        }

        return resultado;
    }

    private char calcularDigitoVerificador(String cuerpo) {
        int suma = 0;
        int multiplicador = 2;

        // Calcular la suma ponderada de los dígitos del cuerpo del RUT
        for (int i = cuerpo.length() - 1; i >= 0; i--) {
            int digito = Character.getNumericValue(cuerpo.charAt(i));
            suma += digito * multiplicador;

            multiplicador++;
            if (multiplicador > 7) {
                multiplicador = 2;
            }
        }

        // Calcular el dígito verificador
        int resto = suma % 11;
        int diferencia = 11 - resto;

        return (diferencia == 11) ? '0' : (diferencia == 10) ? 'K' : Character.forDigit(diferencia, 10);
    }


    public Map<String, String> validarEdad(int edad) {
        Map<String, String> resultado = new HashMap<>();

        if (edad <= 18) {
            resultado.put("estado", "La edad debe ser mayor a 18");
        } else {
            resultado.put("estado", "valido");
        }

        return resultado;
    }
}
