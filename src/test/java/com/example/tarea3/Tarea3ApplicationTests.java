package com.example.tarea3;

import com.example.tarea3.utils.ValidarUsuario;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
public class Tarea3ApplicationTests {

    ValidarUsuario validarUsuario;
    @BeforeEach
    void setUp() {
       validarUsuario = new ValidarUsuario();
    }

    @Test
    public void testNombreNoDebeContenerNumeros() {
        String nombre = "John";
        Map<String, String> resultado = validarUsuario.validarNombre(nombre);
        assertEquals("valido", resultado.get("estado"));
    }


    @Test
    public void numeroValido() {
        String numero = "+56 9 57155449";
        Map<String, String> resultado = validarUsuario.validarNumero(numero);
        assertEquals("valido", resultado.get("estado"));
    }

    @Test
    public void testRutDebeTenerFormatoValido() {
        String rut = "20.835.763-8";
        Map<String, String> resultado = validarUsuario.validarRut(rut);
        assertEquals("valido", resultado.get("estado"));
    }

    @Test
    public void testEdadDebeSerMayorDe18() {
        int edad = 20;
        Map<String, String> resultado = validarUsuario.validarEdad(edad);
        assertEquals("valido", resultado.get("estado"));
    }

}
